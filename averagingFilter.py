import cv2
import os
import numpy as np
from matplotlib import pyplot as plt
import spasavanjeSlika as spasiSliku



def averagingFilter(image,i,slika):
    img = image.copy()
    kernel = np.ones((5,5),np.float32)/25
    dst = cv2.filter2D(img,-1,kernel)

    spasiSliku.spasiSliku('averagingFilter', slika, i, dst)

slike_folder = os.listdir("images/barack_obama")

i = 0
for slike in slike_folder:
    image = cv2.imread("images/barack_obama/" + slike)
    averagingFilter(image,i, "obama")
    i += 1

slike_folder = os.listdir("images/mark_zuckerberg")
i = 0
for slike in slike_folder:
    image = cv2.imread("images/mark_zuckerberg/" + slike)
    averagingFilter(image,i, "mark")
    i += 1    

slike_folder = os.listdir("images/jimmy_kimmel")
i = 0
for slike in slike_folder:
    image = cv2.imread("images/jimmy_kimmel/" + slike)
    averagingFilter(image,i, "kimmel")
    i += 1       