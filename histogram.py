import cv2
import os
import spasavanjeSlika as spasiSliku
from matplotlib import pyplot as plt
from PIL import Image, ImageEnhance


def histogram(image, i, slika):

    img = image
    lab= cv2.cvtColor(img, cv2.COLOR_BGR2LAB)
    l, a, b = cv2.split(lab)

    clahe = cv2.createCLAHE(clipLimit=3.0, tileGridSize=(8,8))
    cl = clahe.apply(l)
    limg = cv2.merge((cl,a,b))
    final = cv2.cvtColor(limg, cv2.COLOR_LAB2BGR)
    spasiSliku.spasiSliku("Histogram", slika, i, final)    


slike_folder = os.listdir("images/barack_obama")

i = 0
for slike in slike_folder:
    image = cv2.imread("images/barack_obama/" + slike)
    histogram(image,i, "obama")
    i += 1

slike_folder = os.listdir("images/mark_zuckerberg")
i = 0
for slike in slike_folder:
    image = cv2.imread("images/mark_zuckerberg/" + slike)
    histogram(image,i, "mark")
    i += 1    

slike_folder = os.listdir("images/jimmy_kimmel")
i = 0
for slike in slike_folder:
    image = cv2.imread("images/jimmy_kimmel/" + slike)
    histogram(image,i, "kimmel")
    i += 1       
