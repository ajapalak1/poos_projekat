import random as random
import os
import cv2

folder="images/barack_obama"
slika="obama"

if (os.path.isdir("Train") == False):
	os.mkdir("Train")

if (os.path.isdir("Test") == False):
	os.mkdir("Test")

#TestObama
for i in range(1,10):
	I = cv2.imread("{}/{}{}.jpg".format(folder, slika, i))
	cv2.imwrite('Test/{}{}.jpg'.format(slika, i), I)


#TrainObama
for i in range(10,18):
	I = cv2.imread("{}/{}{}.jpg".format(folder, slika, i))
	cv2.imwrite('Train/obama/{}{}.jpg'.format(slika, i), I)

folder="images/jimmy_kimmel"
slika="kimmel"

#TestKimmel
for i in range(1,10):
	I = cv2.imread("{}/{}{}.jpg".format(folder, slika, i))
	cv2.imwrite('Test/{}{}.jpg'.format(slika, i), I)


#TrainKimmel
for i in range(10,18):
	I = cv2.imread("{}/{}{}.jpg".format(folder, slika, i))
	cv2.imwrite('Train/kimmel/{}{}.jpg'.format(slika, i), I)



folder="images/mark_zuckerberg"
slika="mark"

#TestMark
for i in range(1,10):
	I = cv2.imread("{}/{}{}.jpg".format(folder, slika, i))
	cv2.imwrite('Test/{}{}.jpg'.format(slika, i), I)


#TrainMark
for i in range(10,18):
	I = cv2.imread("{}/{}{}.jpg".format(folder, slika, i))
	cv2.imwrite('Train/mark/{}{}.jpg'.format(slika, i), I)